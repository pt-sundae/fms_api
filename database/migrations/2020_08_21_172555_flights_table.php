<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FlightsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('flights', function (Blueprint $table) {
            $table->increments('id');
            $table->string('airlines');
            $table->string('airlinesCode');
            $table->string('flightNumber');
            $table->string('departurePort');
            $table->string('arrivalPort');
            $table->dateTimeTz('DepartementTime');
            $table->dateTimeTz('arrivalTime');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('flights');
    }
}
