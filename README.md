## Konfigurasi untuk local

- Setting env. untuk nama database, username, password dll. untuk contoh ada di file env.example
- Setelah selesai setting env buka cmd dan arahkan kefolder project. lalu jalankan perintah "composer install" & "php artisan migrate".
- dikarenakan data kosong, gunakan fungsi store dengan end point http://localhost/fms_api/public/flights/store method post dan format json :
{
    "id": 1,
    "airlines":"Cathay Pacific",
    "airlinesCode":"CX",
    "flightNumber": "QF401",
    "departurePort": "MEL",
    "arrivalPort": "SYD",
    "DepartementTime": "2020-08-22 00:00:00",
    "arrivalTime": "2020-08-23 17:11:11"
}
- setelah berhasil, gunakan end point http://localhost/fms_api/public/flights method get untuk mengambil semua data yang ada didatabase
- untuk mengambil beberapa data bisa gunakan end point http://localhost/fms_api/public/flights/filter method post dengan format json :
{
	"airlinesCode":["EK", "QF", "CX"]
}


