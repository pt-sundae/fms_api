<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\flights;
use Carbon\Carbon;

class FlightsController extends Controller
{
    public $main_model;

    public function __construct(flights $main_model)
    {
        $this->main_model   = $main_model;
    }

    public function index(Request $request)
    {
        $flights = $this->main_model->get();

        return Response()->json($flights, 200);

    }

    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), $this->main_model->rules());
        if($validator->fails()) {
            return Response()->json(
                ['message' => $validator->errors()->all()
            ], 500);
        }

        $data = $this->main_model->create($request->all());

        if(!$data) {
            return Response()->json(
                ['message' => 'Error saving!'
            ], 500);
        }
 
        return Response()->json(
            ['message' => 'Store Successfully!'
        ], 201);
    }

    public function filter(Request $request)
    {
        return $data = $this->main_model->whereIn('airlinesCode', $request->airlinesCode)->get();

        if(is_null($data)) {
            return Response()->json(
                ['message' => 'Not found!'
            ], 404);
        }
     
        return Response()->json($data, 200);
    }
}
