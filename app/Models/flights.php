<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Flights extends Model
{
    /**
    * Table database
    */

    protected $table = 'flights';


    /**
       * The attributes that are mass assignable.
       *
       * @var array
       */
      protected $fillable = [
        'airlines','airlinesCode','flightNumber','departurePort','arrivalPort','DepartementTime','arrivalTime'
      ];

    public static function rules($id='') {
        return [
            'airlines'     => 'required',
            'airlinesCode'    => 'required',
            'flightNumber'     => 'required',
            'departurePort'    => 'required',
            'arrivalPort'      => 'required',
            'DepartementTime'  => 'required',
            'arrivalTime'      => 'required'
        ];
    }

}
